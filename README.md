# Web Scrapping using bs4 
( *submitted as my NIIT project* )

## The project is created using various modules like : 

``` Python
    from openpyxl import load_workbook
    from openpyxl.chart import PieChart3D,Reference
    import re
    from bs4 import *
    import urllib.request
```

*   <b>openpyxl is used to read a excel file specified by the user</b> <br/>
*   <b>re ( regular expression ) is used to validate the url specified in excel file </b> <br/>
*   <b>bs4 is used for web scrapping</b> <br/>


# how to use the code ?
*    The code does not require anything to be changed. Just clone the project and start working.
*    Start the project on command line.
*    And just specify the excel file.

``` Python
python SEO_tool.py
```

# Images : 

## The excel file looks like this before it is given as an input :
<img src = "/before.PNG" />

## When you run the program, just specify all the required parameters which are asked.
<img src = "/console.PNG"/>

## The excel file will appear like this after executing the program
<img src = "/after.PNG" />

## Try with different urls.

<h1> Thank You ! </h1>